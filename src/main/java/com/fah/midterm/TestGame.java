package com.fah.midterm;

public class TestGame {
    public static void main(String[] args) {
        Game game1 = new Game(100, 100, 100);
        Game game2 = new Game(150, 50, 150);
        Game game3 = new Game(200, 10, 0);
        game1.work();
        game1.print();
        System.out.println("--------------");
        game1.sleep();
        game1.print();
        System.out.println("--------------");
        game3.play();
        game3.print();

    }
}
