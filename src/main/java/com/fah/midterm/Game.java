package com.fah.midterm;

public class Game {
    private int hp = 100 ;
    private int money = 100 ;
    private int happy = 100 ;

    public Game(int hp , int money , int happy){
        this.hp = hp;
        this.money = money ;
        this.happy = happy;
    }
    public int getHp(){
        return hp ;
    }
    public int getMoney(){
        return money ;
    }
    public int getHappy(){
        return happy ;
    }
    public boolean setHp(int hp){
        if(hp < 0 ) return false ;
        this.hp = hp ;
        return true ;
    }
    public boolean setmoney(int money){
        if(money < 0 ) return false ;
        this.money = money ;
        return true ;
    }
    public boolean setHappy(int happy){
        if(money < 0 ) return false ;
        this.happy = happy ;
        return true ;
    }
    public void work(){
        money = money + 20 ;
        hp = hp - 10 ;
        happy = happy + 10 ;
    }
    public void sleep(){
        money = money - 10 ;
        hp = hp + 20 ;
        happy = happy + 10 ;
    }
    public void play(){
        money = money - 10 ;
        hp = hp - 10 ;
        happy = happy + 20 ;
    }
    public void print(){
        System.out.println("HP = "+hp);
        System.out.println("Money = "+money);
        System.out.println("Happy = "+happy);
    }
}
